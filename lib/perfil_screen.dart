import 'package:flutter/material.dart';

class PerfilScreen extends StatefulWidget {
  @override
  _PerfilScreenState createState() => _PerfilScreenState();
}

class _PerfilScreenState extends State<PerfilScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
              border: Border(
            top: BorderSide(color: Colors.purple),
          )),
          child: Image.network(
            "",
            fit: BoxFit.contain,
          ),
        ),
        Text("Banco 260 - Nu Pagamentos S.A."),
        Text("Agência 0001"),
        Text("Conta 9999999-1"),
        _buildListTile(icon: Icons.help_outline, title: "Me Ajude"),
        _buildListTile(
            icon: Icons.person_outline,
            title: "Perfil",
            subtitle: "Nome de preferência, telefone, e-mail"),
        _buildListTile(
            icon: Icons.monetization_on, title: "Configurar NuConta"),
        _buildListTile(
            icon: Icons.phonelink_setup, title: "Confgurações do app"),
      ],
    );
  }

  Widget _buildListTile({IconData icon, String title, String subtitle}) {
    return ListTile(
      leading: Icon(
        icon,
        color: Colors.white,
      ),
      title: Text(title),
      subtitle: Text(subtitle),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: () {},
    );
  }
}
