import 'package:flutter/material.dart';
import 'package:nubank/top_bar.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() {
    return new HomeState();
  }
}

class HomeState extends State<Home> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(129, 38, 157, 1),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(
                height: 140,
              ),
              Expanded(child: _buildCenterList(context)),
              _buildSelectedItemFromList(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 130,
                padding: EdgeInsets.only(bottom: 15, top: 15),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child:
                          _buildBottomCard(Icons.group_add, "Indicar amigos"),
                    ),
                    _buildBottomCard(Icons.question_answer, "Cobrar"),
                    _buildBottomCard(Icons.mood, "Depositar"),
                    _buildBottomCard(Icons.attach_money, "Transferir"),
                    _buildBottomCard(Icons.cloud_queue, "Nuvem"),
                  ],
                ),
              )
            ],
          ),
          TopBar(),
        ],
      ),
    );
  }


  Widget _buildCenterList(context) {
    return PageView(
      scrollDirection: Axis.horizontal,
      onPageChanged: (page) {
        setState(() {
          currentPage = page;
        });
      },
      children: <Widget>[
        _buildCenterCard(context),
        _buildCenterCard(context),
        _buildCenterCard(context),
      ],
    );
  }

  Widget _buildCenterCard(context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Card(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            Container(
                                alignment: Alignment.topLeft,
                                padding: EdgeInsets.only(top: 25, left: 25),
                                child: Icon(
                                  Icons.credit_card,
                                  size: 35,
                                )),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(top: 45, left: 25),
                                  child: Text(
                                    "FATURA ATUAL",
                                    style: TextStyle(
                                      color: Colors.lightBlueAccent,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5, left: 25),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        "R\$ ",
                                        style: TextStyle(
                                          color: Colors.lightBlueAccent,
                                          fontSize: 47,
                                        ),
                                      ),
                                      Text(
                                        "593",
                                        style: TextStyle(
                                            color: Colors.lightBlueAccent,
                                            fontSize: 47,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        ",22",
                                        style: TextStyle(
                                          color: Colors.lightBlueAccent,
                                          fontSize: 47,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5, left: 25),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        "Limite disponível ",
                                        style: TextStyle(
                                          fontSize: 20,
                                        ),
                                      ),
                                      Text(
                                        "R\$ 541,60",
                                        style: TextStyle(
                                          color: Colors.lightGreen,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      _buildCardBarraLateral()
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width - 50,
                height: 100,
                color: Colors.grey[100],
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 15),
                child: ListTile(
                  leading: Icon(Icons.local_pizza),
                  title: Text(
                      "Compra mais recente em Pag*Sovetesrequibom no valor de R\$ 6,30 terça"),
                  trailing: IconButton(
                    icon: Icon(Icons.chevron_right),
                    onPressed: () {},
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCardBarraLateral() {
    return Container(
      margin: EdgeInsets.all(25),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                width: 10,
                color: Colors.orange,
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                width: 10,
                color: Colors.lightBlueAccent,
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                width: 10,
                color: Colors.lightGreen,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSelectedItemFromList() {
    Widget itemPage() {
      //  _centerPageController.page
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 3),
        child: CircleAvatar(
          radius: 3,
          backgroundColor: Colors.white,
        ),
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 3),
          child: CircleAvatar(
            radius: 3,
            backgroundColor: currentPage == 0
                ? Colors.white
                : Color.fromRGBO(145, 64, 169, 1),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 3),
          child: CircleAvatar(
            radius: 3,
            backgroundColor: currentPage == 1
                ? Colors.white
                : Color.fromRGBO(145, 64, 169, 1),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 3),
          child: CircleAvatar(
            radius: 3,
            backgroundColor: currentPage == 2
                ? Colors.white
                : Color.fromRGBO(145, 64, 169, 1),
          ),
        ),
      ],
    );
  }

  Widget _buildBottomCard(IconData icon, String text) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Color.fromRGBO(145, 64, 169, 1),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(
                icon,
                color: Colors.white,
              ),
              Text(
                text,
                style: TextStyle(color: Colors.white, fontSize: 17),
              )
            ],
          ),
        ),
      ),
    );
  }
}
