import 'package:flutter/material.dart';

class TopBar extends StatefulWidget {
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> with TickerProviderStateMixin {
  AnimationController controller;
  Animation animTop;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );

    // animTop = Tween(begin: 0.0, end: MediaQuery.of(context).size.height - 160)
    //     .animate(controller);

    animTop =
        Tween(begin: 0.0, end: MediaQuery.of(context).size.height - 160)
            .animate(
      CurvedAnimation(
          parent: controller,
          curve: Curves.bounceOut,
          reverseCurve: Curves.bounceIn),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animTop,
        builder: (context, child) {
          return Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                padding: EdgeInsets.only(top: 45),
                child: Image.asset("images/logo.png"),
              ),
              Container(
                padding: EdgeInsets.all(25),
                height: animTop.value,
                color: Color.fromRGBO(129, 38, 157, 1),
                child: ListView(
                  children: <Widget>[
                    _buildTopListItem(Icons.group_add, "Indicar amigos"),
                    _buildTopListItem(Icons.question_answer, "Cobrar"),
                    _buildTopListItem(Icons.mood, "Depositar"),
                    _buildTopListItem(Icons.attach_money, "Transferir"),
                    _buildTopListItem(Icons.cloud_queue, "Nuvem"),
                  ],
                ),
              ),
              GestureDetector(
                onTap: toggleTopAnimation,
                // onVerticalDragUpdate: (details) {
                //   controller.value += details.primaryDelta /
                //       (MediaQuery.of(context).size.height - 160);
                // },
                // onVerticalDragEnd: (details) {
                //   if (controller.value > 0.5) {
                //     controller.forward();
                //   } else {
                //     controller.reverse();
                //   }
                // },
                child: Container(
                  width: double.infinity,
                  color: Color.fromRGBO(129, 38, 157, 1),
                  child: Icon(
                    animTop.value > 100
                        ? Icons.keyboard_arrow_up
                        : Icons.keyboard_arrow_down,
                    color: Colors.white,
                    size: 35,
                  ),
                ),
              ),
            ],
          );
        });
  }

  Widget _buildTopListItem(IconData icon, String text) {
    return ListTile(
      leading: Icon(
        icon,
        color: Colors.white,
        size: 30,
      ),
      title: Text(
        text,
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
    );
  }

  void toggleTopAnimation() {
    if (controller.value == 0)
      controller.forward();
    else
      controller.reverse();
  }
}
